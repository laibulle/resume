RESUME_SOURCE_FILE=cv.tex
BUILD_PATH=dist

deps:
	brew cask install mactex

build:
	mkdir -p $(BUILD_PATH)
	xelatex --output-directory $(BUILD_PATH) $(RESUME_SOURCE_FILE) 

clean:
	-@ rm -R $(BUILD_PATH)

.PHONY: clean
